<!DOCTYPE html>
<html lang="es">	
	<head>
		<meta charset="utf8">
		<link rel="stylesheet" href="../css/style.css">
		<title>
		
		</title>
	</head>
	<body>
		
		<header>
	    <img src="../img/encabezado.bmp" width="100%" height="170"></header>
		
		<nav> 
			<ul>
				<li><a href="../index.php">Inicio</a></li>
				<li><a href="bautisos.php">Bautismo</a></li>
                <li><a href="comunion.php">Comunion</a></li>
				<li><a href="confirmacion.php">Confirmacion</a></li>
                <li><a href="matrimonio.php">Matrimonio</a></li>
                <li><a href="eucaristia.php">Eucarisitia</a></li>
                
			</ul>
		</nav>
		
		<section id="contenedor">
		<section id="principal">
			<article>
				<p><h2> Requisitos Bautismo </h2></p>
		
          
          
           <h4><p> 
           
       <h3> Hijos Legitimos</h3>

<li>Registro Civil del menor que va a ser Bautizado (original)</li>
<li>Partida de Matrimonio de los padres (original)</li>
<li>Fotocopia de la cedula de los padres y padrinos</li> 

 <h3>Hijos Reconocidos</h3>
<li>Registro Civil del menor que va a ser Bautizado (original)</li>
<li>Partida de Bautismo de los padres (originales)</li>
<li>Fotocopia de la cedula de los padres y padrinos</li>

 <h3>Hijos De Madre Soltera </h3>
<li>Registro Civil del menor que va a ser Bautizado (original)</li>
<li>Partida de Bautismo de la madre (original)</li>
<li>Fotocopia de la cedula de la madre y padrinos</li>

 <h3>Los Padrinos</h3>
<li>Haber cumplido minimo 16 años</li>
<li>Estar confirmado y llevar una vida congruente con la Fe y con la mision que va a asumir</li>
<li>No estar afectado por una pena canonica legitimamente impuesta o declarada.</li>
<li>No ser el padre o la madre de quien se va a bautizar</li>

 <h3>Valor De Los Servicios (Año 2013)</h3>
<li>Estipendio sacerdote: Donacion (a consideracion Padres y Padrinos)</li>
<li>Reserva iglesia: Cuando el Bautizo no es Comunitario se reserva el templo con 		una donación</li>
<br>

      
        </p></h4>
        
        
           
            </article>
			</section>
			<aside>
             	<br>
				
     <br>
      <br>
      
      <p><h3 align="center">Informacion Bautisos</h3> 

     <ul>


                        <li><a href="requisitosbautisos.php">Requisitos</a></li>
                        <li><a href="inscripciones.php">Inscripciones</a></li>
                        <li><a href="fechasbautisos.php">Fechas Celebracion</a></li>
                        <li><a href="consultas.php">Consulta Partida</a></li>
                        
					</ul>
      <br>
      <center><img src="../img/cruz.jpg" width="160" height="180"  longdesc="img/news.jpg"></center>
       <br>
   <h5><p>  
   
   "Yo a la verdad os bautizo en agua para arrepentimiento; pero el que viene tras mi, cuyo calzado no soy digno de llevar, es más poderoso que yo; él os bautizará en Espíritu Santo y fuego".(Mateo 3:11)</p></h5> 
    
			
            <br>
            
 
			</aside>
		</section>
		<footer>
			<h4>Desarrollo Web 2013 -1
            <br>
            William Jaraba Morelo - Lewys Correa Sanchez
            <br>
            Ingenieria de Sistemas - Universidad De Cordoba</h4>  
		</footer>
	</body>
</html>