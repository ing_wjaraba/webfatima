<!DOCTYPE html>
<html lang="es">	
	<head>
		<meta charset="utf8">
		<link rel="stylesheet" href="../css/style.css">
		<title>
		
		</title>
	</head>
	<body>
		
		<header>
	    <img src="../img/encabezado.bmp" width="100%" height="170"></header>
		
		<nav> 
			<ul>
				<li><a href="../index.php">Inicio</a></li>
				<li><a href="bautisos.php">Bautismo</a></li>
                <li><a href="comunion.php">Comunion</a></li>
				<li><a href="confirmacion.php">Confirmacion</a></li>
                <li><a href="matrimonio.php">Matrimonio</a></li>
                <li><a href="eucaristia.php">Eucarisitia</a></li>
                
			</ul>
		</nav>
		
		<section id="contenedor">
		<section id="principal">
			<article>
		
		
          
          
           <h4><p> 
           
       <p><h2>Requisitos  Para Contraer Matrimonio </h2></p>
       <br>
       
<li>Partidas de Bautismo ORIGINALES de los Contrayentes, con una expedición no mayor a tres (3) meses. Si la Partida de Bautismo fuera de otra Diócesis, debe estar Autenticada por la Diócesis respectiva.</li>

<li>Partida de Confirmación Original de los Contrayentes (si no aparece la nota marginal en la partida de Bautismo)</li>

<li>Certificado de Cursillo Prematrimonial aprobado por la Arquidiocesis de Medellín (original)</li>

<li>Certificado de Solteria del Contrayente que no resida en el sector de la Parroquia donde se hace el Expediente Matrimonial</li>

<li>Fotocopia de la Cedula de cada Contrayente.</li>

<li>Una Foto de cada Contrayente o Dos Fotos de cada persona cuando se casan en otra parroquia.</li>

<li>Si alguno de los novios ha estado casado por lo católico debe traer la anulación del matrimonio. Y si ha estado casado por lo civil debe traer el registro de divorcio.(Originales)</li>

<li>Certificado de Defunción del cónyuge, si uno de los novios es viudo.
Si la pareja tiene hijos y los quiere legitimar, debe traer la partida de Bautismo de los hijos.(Originales)</li>

<li>Los contrayentes deben traer todos los papeles mínimo un mes antes de la ceremonia y pedir una cita para la elaboración del Expediente Matrimonial y del Nihil Obstat. Y debe asistir el día de la cita para la elaboración del expediente con dos testigos que los conozcan.</li>

<h3>Valoe De Los Serivicos (año 2013)</h3>

<li>Expediente y Nihil Obstat $ 60.000</li>
<li>Nihil Obstat recibido $ 45.000 </li>
<li>Estipendio Sacerdote: A consideración de los contrayentes.</li>
<br>

      
        </p></h4>
           
            </article>
			</section>
            <aside>
			<br>
				
     <br>
      <br>
      
      <p><h3 align="center">Informacion Matrimonios</h3> 

     <ul>


                        <li><a href="requisitosmatrimonio.php">Requisitos</a></li>
                        <li><a href="inscripciones.php">Inscripciones</a></li>
                        <li><a href="fechasmatrimonio.php">Fechas Celebracion</a></li>
                        <li><a href="consultas.php">Consulta Partida</a></li>
                 
					</ul>
      <br>
      <center><img src="../img/cruz.jpg" width="160" height="180"  longdesc="img/news.jpg"></center>
       <br>
   <h5><p>  
   
   “Así que no son ya más dos, sino una sola carne; por tanto, lo que Dios juntó, no lo separe el hombre” (Mateo 19:6).
</p></h5> 
    
    <h5><p>  
   “Por esto dejará el hombre a su padre y a su madre, y se unirá a su mujer, y los dos serán una sola carne.” (Efesios 5:31)

</p></h5> 
			
            <br>
            
 
			</aside>
		</section>
		<footer>
		<h4>Desarrollo Web 2013 -1
            <br>
            William Jaraba Morelo - Lewys Correa Sanchez
            <br>
            Ingenieria de Sistemas - Universidad De Cordoba</h4>  
		</footer>
	</body>
</html>