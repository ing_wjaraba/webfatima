<!DOCTYPE html>
<html lang="es">	
	<head>
		<meta charset="utf8">
		<link rel="stylesheet" href="../css/style.css">
		<title>
		
		</title>
	</head>
	<body>
		
		<header>
	    <img src="../img/encabezado.bmp" width="100%" height="170"></header>
		
		<nav> 
			<ul>
				<li><a href="../index.php">Inicio</a></li>
				<li><a href="bautisos.php">Bautismo</a></li>
                <li><a href="comunion.php">Comunion</a></li>
				<li><a href="confirmacion.php">Confirmacion</a></li>
                <li><a href="matrimonio.php">Matrimonio</a></li>
                <li><a href="eucaristia.php">Eucarisitia</a></li>                
			</ul>
		</nav>
		
		<section id="contenedor">
		<section id="principal">
			<article>
				<p><h2> Primera Comunión</h2></p>
		
          
          
           <h4><p>La Primera Comunión es uno de los siete sacramentos de la Iglesia Católica, y uno de los tres sacramentos de iniciación a la vida cristiana junto al bautismo y a la confirmación. A través de la primera comunión, luego de cierta preparación llamada catequesis, en la que se estudia el catecismo, es posible tomar por primera vez la hostia y el vino, que para quienes profesan esta religión, no simboliza, sino que es el Cuerpo y la Sangre de Cristo. La presencia real del cuerpo y la sangre de Cristo en el pan y el vino es lo que en la Iglesia Católica se denomina Transubstanciación
           <br>
           <br>
           Este sacramento fue instituido por Jesús durante la última cena, quien en compañía de sus discípulos tomó el pan y el vino y dijo: “Yo soy el pan de la vida, si uno come de este pan vivirá para siempre, pues el pan que yo os daré es mi carne, para la vida del mundo" (Jn.6,32-34, 51). "El que come mi carne y bebe mi sangre tiene vida eterna".(Jn 6,54)
        </p></h4>
        
          <center><img src="../img/comulgar.jpg" width="450" height="270"  longdesc="img/news.jpg"></center>
           
            </article>
			</section>
            <aside>
			<br>
				
     <br>
      <br>
      
      <p><h3 align="center">Informacion Comunión </h3> 

     <ul>


                        <li><a href="requisitoscomunion.php">Requisitos</a></li>
                      
                        <li><a href="fechascomunion.php">Fechas Celebracion</a></li>
                       
                 
					</ul>
      <br>
      <center><img src="../img/cruz.jpg" width="160" height="180"  longdesc="img/news.jpg"></center>
       <br>
   <h5><p>  
   
   "El que comiere el pan y bebiere el cáliz del Señor indignamente, será reo del Cuerpo y de la Sangre del Señor; porque el que come y bebe, no haciendo distinción del Cuerpo del Señor, come y bebe su propia condenación"(I Cor 11:27)</p></h5> 
    
			
            <br>
            
 
			</aside>
		</section>
		<footer>
	<h4>Desarrollo Web 2013 -1
            <br>
            William Jaraba Morelo - Lewys Correa Sanchez
            <br>
            Ingenieria de Sistemas - Universidad De Cordoba</h4>    
		</footer>
	</body>
</html>