<!DOCTYPE html>
<html lang="es">	
	<head>
		<meta charset="utf8">
		<link rel="stylesheet" href="../css/style.css">
		<title>
		
		</title>
	</head>
	<body>
		
		<header>
	    <img src="../img/encabezado.bmp" width="100%" height="170"></header>
		
		<nav> 
			<ul>
				<li><a href="../index.php">Inicio</a></li>
				<li><a href="bautisos.php">Bautismo</a></li>
                <li><a href="comunion.php">Comunion</a></li>
				<li><a href="confirmacion.php">Confirmacion</a></li>
                <li><a href="matrimonio.php">Matrimonio</a></li>
                <li><a href="eucaristia.php">Eucarisitia</a></li>                
			</ul>
		</nav>
		
		<section id="contenedor">
		<section id="principal">
			<article>
		
           <h4><p>
           
           <h3> Requisitos Primera Comunión</h3>
         
           <li> Tener por lo menos 8 años de edad, saber leer y escribir, y estar en tercer grado como mínimo.</li> 
<br>
<li> Presentar fe de bautismo o certificado de bautismo de acuerdo con la fe en Dios 
  Padre, Hijo y Espíritu Santo y una copia del acta de nacimiento.</li>
<br>
<li> Los aspirantes debe realizar un curso de catequesis  de claseslos domungos en nuestra  iglesia. Deberán memorizar y aprender las bases de la fe cristiana y el significado de la Santa Comunión. </li>
<br>
<li> Padres y estudiantes deberán participar en las Misas todos los domingos y ser 
activos en la vida de la congregación</li>

<br>
<li> Los padres se comprometen a asegurarse que los niños y niñas aprendan las 
lecciones y memorizen las oraciones: Padre Nuestro, Credo y los Diez Mandamientos.</li>
<br>
<li> Antes de la celebración de la Primera Comunión deberán tener planeado 
quienes serán los padrinos que representen al candidato en su Primera Comunión. 
Las personas escogidas deberán haber sido bautizadas, y haber hecho su Primera 
Comunión. Deberán escoger personas que puedan ser ejemplos de la fe y  conducta para los niños.</li>
<br>
<li> La vestimenta de las niñas puede ser todo de blanco, y para los niños un 
pantalón azul oscuro o negro, camisa blanca, y corbata del mismo color del 
pantalón. Esta vestimenta no es un requisito para recibir el sacramento pero es la 
tradición.</li>
<br>

<li> Los padres y padrinos acostumbran traer una contribución especial por cada 
niño el día de la Misa de Primera Comunión en agradecimiento a Dios por lo que 
está haciendo en la vida de su hijo/a. Esto debe hacerse como un acto de 
adoración, igual que cada Domingo, al recibir el cesto de la ofrenda</li>
        </p></h4>
        
        
           
            </article>
			</section>
            <aside>
			<br>
				
     <br>
      <br>
      
      <p><h3 align="center">Informacion Comunión </h3> 

     <ul>


                         <li><a href="requisitoscomunion.php">Requisitos</a></li>
                  
                        <li><a href="fechascomunion.php">Fechas Celebracion</a></li>
               
                 
					</ul>
      <br>
      <center><img src="../img/cruz.jpg" width="160" height="180"  longdesc="img/news.jpg"></center>
       <br>
   <h5><p>  
   
   "El que comiere el pan y bebiere el cáliz del Señor indignamente, será reo del Cuerpo y de la Sangre del Señor; porque el que come y bebe, no haciendo distinción del Cuerpo del Señor, come y bebe su propia condenación"(I Cor 11:27)</p></h5> 
    
			
            <br>
            
 
			</aside>
		</section>
		<footer>
		<h4>Desarrollo Web 2013 -1
            <br>
            William Jaraba Morelo - Lewys Correa Sanchez
            <br>
            Ingenieria de Sistemas - Universidad De Cordoba</h4>   
		</footer>
	</body>
</html>